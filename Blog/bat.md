### cat with wings

I recently discovered [bat](https://github.com/sharkdp/bat), a modern `cat` alternative written in, yes, the Rust programming language. It comes with a plethora of features like  syntax highlighting, git integration and and automatic paging which replaces the dull, ugly output of `cat`.

## Installation
The various installation methods are listed in the their [readme](https://github.com/sharkdp/bat#using-bat-on-windows). `bat` works out of the box on every platform. Windows requires extra [configuration](https://github.com/sharkdp/bat#using-bat-on-windows) for a few features.

## Using bat
Like `cat`, `bat` works in the same way so all you have to do is type in
```
bat filename
```
Do note that on some distros like Ubuntu and Debian it is called `batname` to avoid nameclash with another package. You can create an alias to define it back to `bat`.
![[Pasted image 20230614024134.png]]
The decorations can be turned off with `-n` to display only line numbers.
![[Pasted image 20230614032645.png]]
There are tons of different features like printing specified range of lines, setting themes, adding or changing filetype associations, adding new language definitions and changing output styles. It works with a number of other tools like `xclip`, `fd`, `git diff` to enhance their results. To get a list of all the options and commands, run `cat --help` or `man cat` and look at their [docs](https://github.com/sharkdp/bat#how-to-use).
![[Pasted image 20230614042101.png]]
The `git` integration allows you to see modifications in the file with respect to the index.
![[Pasted image 20230615030248.png]]
All your configurations are saved neatly in a config file. To find the path to your `bat.conf` and making changes to it, run `bat --config-file`.
![[Pasted image 20230614041251.png]]
`bat` is a cool, useful, and really fast tool for you to try out and maybe even switch to. A shoutout to the author [sharkdp](https://github.com/sharkdp) who has also authored popular and open source tools like [fd](https://github.com/sharkdp/fd), [hyperfine](https://github.com/sharkdp/hyperfine), [pastel](https://github.com/sharkdp/pastel) and many, many more. Checkout his [website](https://david-peter.de/) and be amazed!

That's all I have for you today and I will see you in the next one. 