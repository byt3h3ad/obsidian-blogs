#### What is Django REST framework?
Well, it's a toolkit that allows you to build web APIs. Okay, so think of it as a standalone API where your clients can make requests such as get, post, put, delete requests to you. Your API receives this information, it validates the information and it responds to the user.

